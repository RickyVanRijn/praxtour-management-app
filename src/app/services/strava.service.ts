import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Storage} from "@ionic/storage";

@Injectable({ providedIn: 'root' })
export class StravaService {

    clientId:String = "21039";
    clientSecret:String = "9559f494de28a40eb30de415a2b27b84f787c280";

    stravaOauthUrl:string = 'https://www.strava.com/oauth/token';
    stravaActivityUploadUrl:string = 'https://www.strava.com/api/v3/uploads';

    stravaActivityDataType:string = 'gpx';

    authorisationError:string = 'Please authorise Strava for use with this app.';


    constructor( private http:HttpClient, private storage:Storage ) {
        this.storage = storage;
    }

    getStravaAccessToken() {
        return this.refreshStravaToken()
            .then(tokens => {
                return tokens;
            })
            .catch(error => {
                this.getCustomerOAuthToken()
                    .then(customerToken => {
                        alert(customerToken);

                        let postData = {
                            "client_id": this.clientId,
                            "client_secret": this.clientSecret,
                            "code": customerToken,
                            "grant_type": GrantType.AuthorizationCode
                        }

                        this.http.post(this.stravaOauthUrl, postData).subscribe(data => {

                            //Eerste access code van strava voor toegang
                            this.storage.set('accessToken', data['access_token']);
                            //Refresh token voor het in stand houden van sessie
                            this.storage.set('refreshToken', data['refresh_token']);

                            console.log('tokens: '+data['refresh_token'] + ', ' + data['access_token'] + ' received!');
                            return data;
                        }, error => {
                            alert('Error: ' + error.message);
                        });
                    })
                    .catch(() => {
                        alert(this.authorisationError);
                        return;
                    });
        });
    }

    refreshStravaToken() {
        return this.storage.get("refreshToken").then(
            (refreshToken) => {
                console.log(refreshToken);

                let postData = {
                    "client_id": this.clientId,
                    "client_secret": this.clientSecret,
                    "refresh_token":refreshToken,
                    "grant_type": GrantType.RefreshToken
                }

                this.http.post(this.stravaOauthUrl, postData).subscribe(data => {
                    this.storage.set('accessToken', data['access_token']);
                    this.storage.set('refreshToken', data['refresh_token']);
                    return data;
                }, error => {
                    alert('Error: ' + error);
                });
            });
    }

    uploadActivityFileToStrava( filename )  {
        this.storage.get("accessToken").then(
            (accessToken) => {
                console.log(accessToken);
                this.http.get(filename, {responseType: 'blob'})
                    .subscribe(data => {
                        let params = new FormData();
                        params.append('file', data, filename);
                        params.append("data_type", this.stravaActivityDataType );

                        this.http.post( this.stravaActivityUploadUrl , params, { headers:{"Authorization":"Bearer "+accessToken}}).subscribe(data => {
                            this.storage.set('activityId', data['id_str']);
                        }, error => {
                            this.storage.set('activityId', '');
                            alert('Error: '+error.message);
                        });
                    });
            })
            .catch(
                error => {
                    alert(this.authorisationError);
                    return;
                }
            );


    }

    updateUploadActivityStatus()  {
        var statusMessageReady = 'Your activity is ready.';

        Promise.all([this.storage.get("activityId"),this.storage.get("accessToken")]).then(
            values => {
                this.http.get(this.stravaActivityUploadUrl+'/' + values[0], {headers:{"Authorization":"Bearer " + values[1]}})
                    .subscribe(
                        data => {
                            let error = data['error'];
                            let status = data['status'];
                            if (error.toString().length == 0) {
                                this.storage.set('activityStatus', error);
                            } else {
                                this.storage.set('activityStatus', status);
                            }
                        } ,
                        error => {console.log('Error: '+ error.message);}
                    );
            });
    }

    private getCustomerOAuthToken() : Promise<any> {
        return this.storage.get("basicToken").then(
            (customerTokenvalue) => {
                console.log('CustomerOauthStartToken: '+ customerTokenvalue);

                if ( customerTokenvalue.trim().length == 0 ) {
                    return '';
                } else {
                    return customerTokenvalue;
                }
            }
        );
    }

}

enum GrantType {
    AuthorizationCode = "authorization_code",
    RefreshToken = "refresh_token"
}
