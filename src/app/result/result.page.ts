import { Component, OnInit } from '@angular/core';
import PraxtourResponseObject = Response.PraxtourResponseObject;
import {StravaService} from "../services/strava.service";
import {Storage} from "@ionic/storage";

@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {

  routeProfile:String;
  routeDate:String;
  routeLevel:String;
  routeTime:String;
  routeCalories:String;
  routeData:PraxtourResponseObject;
  routeSpeed:string;
  routeDistance:String;
  routeHeartrate:String;
  routeRpm:String;
  routeWatts:String;

  activityStatus:string= '';

  constructor(private stravaService:StravaService, private storage:Storage) { }

  ngOnInit() {
    this.routeProfile = 'Betuwe';
    this.routeLevel = 'Real';
    this.routeDate = '10-04-2020';
    this.routeTime = '00:31:44,22';
    this.routeDistance = '16384';
    this.routeSpeed = '18,6';
    this.routeCalories = '1649';
    this.routeRpm = '96,7';
    this.routeHeartrate = '89';
    this.routeWatts = '178';
    // this.routeDistance = this.routeData.TrainingCenterDatabase.Activities.Activity.Lap.DistanceMeters.toString();
    // this.routeRpm = this.routeData.TrainingCenterDatabase.Activities.Activity.Lap.Cadence.toString();
    // this.routeHeartrate = this.routeData.TrainingCenterDatabase.Activities.Activity.Lap.AverageHeartRateBpm.Value.toString();
  }

  upload() {
    this.stravaService.getStravaAccessToken();
    this.stravaService.uploadActivityFileToStrava('./assets/example_data/test.gpx');
    this.stravaService.updateUploadActivityStatus();

    let timerId = setInterval(() =>{
      this.storage.get('activityStatus').then(
          (value) => {
            this.activityStatus = value;
            if (value.toString().includes('error') || value.toString().includes('ready')) {
              clearInterval(timerId);
            }
          }
      );
    }, 3000);
  }

}
