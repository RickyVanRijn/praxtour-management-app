import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceselectionPage } from './deviceselection.page';

describe('Tab2Page', () => {
  let component: DeviceselectionPage;
  let fixture: ComponentFixture<DeviceselectionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DeviceselectionPage],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceselectionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
