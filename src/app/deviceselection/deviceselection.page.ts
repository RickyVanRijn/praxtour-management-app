import {Component, OnInit} from '@angular/core';
import {Storage} from "@ionic/storage";
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'deviceselection.page.html',
  styleUrls: ['deviceselection.page.scss']
})
export class DeviceselectionPage implements OnInit {

  constructor(private router:Router, private storage:Storage) {
  }

  ngOnInit(): void {
  }

  selectDeviceType( deviceType:string ) {
    this.router.navigate(['/tabs/connectdevice', deviceType] );
  }

}
