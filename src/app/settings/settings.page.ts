import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage {

  stravaAuthkey:String;

  constructor(private storage: Storage) {
    this.storage = storage;
    this.storage.get("basicToken").then(
        (basicToken) => {
          this.stravaAuthkey = basicToken;
        });
  }

  authorize() {
    window.open('https://www.strava.com/oauth/authorize?client_id=21039&response_type=code&redirect_uri=http://app.praxtour.com&approval_prompt=force&scope=activity:write,read', '_system');
  }

  storeToken() {
    this.storage.set('basicToken', this.stravaAuthkey.toString());
    console.log(this.stravaAuthkey)
  }

}
