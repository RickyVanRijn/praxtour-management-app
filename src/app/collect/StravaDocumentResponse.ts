export interface StravaDocumentResponse {
  uuid              : string  ;
  responseCode      : number  ;
  documentName      : string  ;
  documentContent   : string  ;
  documentJson      : any     ;
}
