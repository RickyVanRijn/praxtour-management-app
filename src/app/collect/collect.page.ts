import { Component } from '@angular/core';
import { Storage } from '@ionic/storage';

import {StravaDocumentResponse} from "./StravaDocumentResponse";
import PraxtourResponseObject = Response.PraxtourResponseObject;

import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-tab1',
  templateUrl: 'collect.page.html',
  styleUrls: ['collect.page.scss']
})
export class CollectPage {
    json:StravaDocumentResponse;
    jsonResponse:PraxtourResponseObject;
    host:string = 'http://192.168.178.80';

    constructor( private storage:Storage, private http:HttpClient) {
    }

    getRoute() {
        this.getDocument();
        this.getJson();
        this.storeResult();
    }

    getDocument() {
        this.http.get(this.host+':1515/currentDocument', {})
            .subscribe(data => {
                this.json = JSON.parse(data.toString());

                if ( this.json.responseCode == 200 ) {
                    //TODO inform user json is accepted or remove if statement.
                }
            });
    }

    getJson() {
        this.http.get(this.host + ':1515/currentJson', {})
            .subscribe(data => {
                this.jsonResponse = JSON.parse(data.toString());
            });
    }

    // writeFile() {
    //     this.file.createFile(this.file.tempDirectory, this.json.documentName, true);
    //     this.file.writeFile(this.file.tempDirectory, this.json.documentName, this.json.documentContent);
    // }

    storeResult() {
        this.storage.set('uuid', this.json.uuid);
        this.storage.set('result_'+this.json.documentName, this.jsonResponse);
    }
}
