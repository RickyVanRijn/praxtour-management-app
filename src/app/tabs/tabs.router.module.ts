import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'collect',
        children: [
          {
            path: '',
            loadChildren: '../collect/collect.module#CollectPageModule'
          }
        ]
      },
      {
        path: 'home',
        children: [
          {
            path: '',
            loadChildren: '../home/home.module#HomePageModule'
          }
        ]
      },
      {
        path: 'deviceselection',
        children: [
          {
            path: '',
            loadChildren: '../deviceselection/deviceselection.module#DeviceselectionPageModule'
          }
        ]
      },
      {
        path: 'results',
        children: [
          {
            path: '',
            loadChildren: '../results/results.module#ResultsPageModule'
          }
        ]
      },
      {
        path: 'result',
        children: [
          {
            path: '',
            loadChildren: '../result/result.module#ResultPageModule'
          }
        ]
      },
      {
        path: 'connectdevice/:deviceType',
        children: [
          {
            path: '',
            loadChildren: '../connectdevice/connectdevice.module#ConnectdevicePageModule'
          }
        ]
      },
      {
        path: 'settings',
        children: [
          {
            path: '',
            loadChildren: '../settings/settings.module#SettingsPageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/tabs/home',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
