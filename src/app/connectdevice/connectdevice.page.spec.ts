import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConnectdevicePage } from './connectdevice.page';

describe('ConnectdevicePage', () => {
  let component: ConnectdevicePage;
  let fixture: ComponentFixture<ConnectdevicePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConnectdevicePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConnectdevicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
