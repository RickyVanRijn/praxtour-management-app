import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-connectdevice',
  templateUrl: './connectdevice.page.html',
  styleUrls: ['./connectdevice.page.scss'],
})
export class ConnectdevicePage implements OnInit {

  deviceType:string = '';

  ctrImgSrc:string = '/assets/connectdevice/ctr.jpg';
  cruiseImgSrc:string = '/assets/connectdevice/cruise.jpg';
  courseImgSrc:string = '/assets/connectdevice/course-basic.jpg';

  imgSrc:string = '';

  constructor(private activatedroute:ActivatedRoute) { }

  ngOnInit() {
      this.activatedroute.paramMap.subscribe(params => {
          console.log(params);
          this.deviceType = params.get('deviceType');
      });

      if (this.deviceType == 'ctr') {
        this.imgSrc = this.ctrImgSrc;
      }
      else if (this.deviceType == 'cruise') {
        this.imgSrc = this.cruiseImgSrc;
      }
      else if (this.deviceType == 'course') {
        this.imgSrc = this.courseImgSrc;
      } else {
        this.imgSrc = this.courseImgSrc;
      }
  }

}
