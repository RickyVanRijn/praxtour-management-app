import { Component } from '@angular/core';
import {StravaService} from "../services/strava.service";
import {Storage} from "@ionic/storage";


@Component({
    selector: 'app-tab3',
    templateUrl: 'results.page.html',
    styleUrls: ['results.page.scss']
})
export class ResultsPage {

    activityStatus:string= '';
    statusColor:string = 'light';

    constructor(private stravaService:StravaService, private storage:Storage) {

    }

    upload() {
        this.stravaService.getStravaAccessToken();
        this.stravaService.uploadActivityFileToStrava('./assets/example_data/test.gpx');
        this.stravaService.updateUploadActivityStatus();

        let timerId = setInterval(() =>{
            this.storage.get('activityStatus').then(
                (value) => {
                    this.activityStatus = value;
                    if (value.toString().includes('error') || value.toString().includes('ready')) {
                        clearInterval(timerId);
                    }
                }
            );
        }, 3000);
    }

}
