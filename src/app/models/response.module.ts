declare module Response {

  export interface MaximumHeartRateBpm {
    Value: number;
  }

  export interface AverageHeartRateBpm {
    Value: number;
  }

  export interface HeartRateBpm {
    Value: number;
  }

  export interface Position {
    LatitudeDegrees: number;
    LongitudeDegrees: number;
  }

  export interface TPX {
    xmlns: string;
    Watts: number;
  }

  export interface Extensions {
    TPX: TPX;
  }

  export interface Trackpoint {
    HeartRateBpm: HeartRateBpm;
    Position: Position;
    SensorState: string;
    Cadence: number;
    Time: Date;
    AltitudeMeters: number;
    DistanceMeters: number;
    Extensions: Extensions;
  }

  export interface Track {
    Trackpoint: Trackpoint[];
  }

  export interface Lap {
    MaximumHeartRateBpm: MaximumHeartRateBpm;
    StartTime: Date;
    Cadence: number;
    DistanceMeters: number;
    TriggerMethod: string;
    TotalTimeSeconds: number;
    AverageHeartRateBpm: AverageHeartRateBpm;
    Track: Track;
  }

  export interface Version {
    VersionMinor: number;
    BuildMinor: number;
    VersionMajor: number;
    BuildMajor: number;
  }

  export interface Creator {
    UnitId: number;
    Version: Version;
    type: string;
    ProductID: number;
    Name: string;
  }

  export interface Activity {
    Sport: string;
    Lap: Lap;
    Id: Date;
    Creator: Creator;
  }

  export interface Activities {
    Activity: Activity;
  }

  export interface Version2 {
    VersionMinor: number;
    BuildMinor: number;
    VersionMajor: number;
    BuildMajor: number;
  }

  export interface Build {
    Type: string;
    Version: Version2;
  }

  export interface Author {
    LangID: string;
    PartNumber: string;
    Build: Build;
    type: string;
    Name: string;
  }

  export interface TrainingCenterDatabase {
    Activities: Activities;
    xmlns: string;
    schemaLocation: string;
    Author: Author;
    xsi: string;
  }

  export interface PraxtourResponseObject {
    TrainingCenterDatabase: TrainingCenterDatabase;
  }

}

